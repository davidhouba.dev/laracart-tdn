@extends('errors::layout')


@section('title', 'Erreur 404')

@section('message')

    <img src="{{asset('/images/pandou.png')}}" alt="">
    <p>Quelque chose à foiré, une erreur 404 s'est pointée</p>

    <p><a href="{{route('home')}}">Retourner à l'accueil</a></p>

@stop
