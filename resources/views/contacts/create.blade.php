@extends('layouts.default', ['title' => 'contact'])

@section('content')

    <div class="row">
        <div class="container mr-auto">

            <h2>Get In Touch</h2>

            <p><span class="text-muted">If you having trouble with this service, please <a href="{{ config('laracarte.admin_support_email') }}" data-helpful="laracarte" data-helpful-modal="on">ask for help</a>.</span></p>

            <form method="POST" action="{{route('contact')}} " novalidate>
                @csrf

                <div class="form-group">
                    <label class="control-label" for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ old('name') }}" required="required">
                    {!! $errors->first('name','<div class="invalid-feedback">:message</div>') !!}

                </div>

                <div class="form-group">
                    <label class="control-label" for="email">Email</label>
                    <input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" required="required">
                    {!! $errors->first('email','<div class="invalid-feedback">:message</div>') !!}

                </div>

                <div class="form-group">
                    <label class="control-label sr-only" for="message">Message</label>
                    <textarea class="form-control {{$errors->has('message') ? 'is-invalid' : '' }}" name="message" id="message" rows="10" cols="10" required="required">{{ old('message') }}</textarea>
                    {!! $errors->first('message','<div class="invalid-feedback">:message</div>') !!}

                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Submit Enquiry &raquo;</button>
                </div>
            </form>
        </div>
    </div>
    </div>

@stop
