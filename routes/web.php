<?php



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'pages.home')->name('home');

Route::view('/about', 'pages.about')->name('about');

Route::name('contact')->get('/contact','ContactsController@create');

Route::name('contact')->post('/contact','ContactsController@store');

Auth::routes(['verify' => true]);

//Route::get('/home', 'HomeController@index')->name('home');
